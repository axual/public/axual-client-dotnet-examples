﻿// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

 using System;
 using System.Collections.Generic;
 using System.IO;
 using Avro;
 using Avro.Generic;
 using Axual.Kafka.Proxy.Proxies.Axual;
 using Axual.SchemaRegistry.Serdes.Avro.Serializers;
 using Confluent.Kafka;
 using Settings;

 namespace axual_client_proxy_generic_avro_producer
 {
     public class Program
     {
         private static RecordSchema _applicationLogEventSchema;
         private static RecordSchema _applicationSchema;

         public static void Main()
         {
             var serverSetting = new ServerSetting(PlatformType.Standalone);

             _applicationSchema = (RecordSchema)
                 Schema.Parse(File.ReadAllText(serverSetting.ApplicationSchemaPath));
             _applicationLogEventSchema = (RecordSchema)
                 Schema.Parse(File.ReadAllText(serverSetting.ApplicationLogEventSchemaPath));

             const string streamName = "applicationlogevents";
             var config = new AxualProducerConfig
             {
                 ApplicationId = "io.axual.example.client.avro.producer",
                 EndPoint = serverSetting.EndPoint,
                 Environment = serverSetting.Environment,
                 Tenant = serverSetting.Tenant,

                 // SSL Settings
                 SecurityProtocol = SecurityProtocol.Ssl,
                 EnableSslCertificateVerification = false,
                 
                 // Server verifies the identity of the client
                 // (i.e. that the public key certificate provided by the client has been signed by a
                 // CA trusted by the server).
                 // Option 1: As path
                 SslCaLocation = serverSetting.SslCaPath,
                 // Option 2: As a PEM file format
                 //SslCaLocation = File.ReadAllText(serverSetting.SslCaPath),
                 
                 // Setting client public and private keys
                 // Option A: Using p12 files
                 SslKeystorePassword = serverSetting.KeystorePassword,
                 SslKeystoreLocation = serverSetting.ProducerKeystoreResourcePath,
                 // Option B: Using PEM files path
                 //SslCertificateLocation =  serverSetting.SslProducerCertificateLocation,
                 //SslKeyLocation =  serverSetting.SslProducerKeyLocation,
                 // Option C: Using PEM format string
                 //SslCertificatePem =  File.ReadAllText(serverSetting.SslProducerCertificateLocation),
                 //SslKeyPem =  File.ReadAllText(serverSetting.SslProducerKeyLocation),
                 
                 //Debug = "all"
             };

             RunProducer(config, streamName);
         }

         private static void RunProducer(ProducerConfig config, string streamName)
         {
             Console.WriteLine();
             Console.WriteLine("-------------------------------------------------------------------------------------");
             Console.WriteLine($"'{typeof(Program).Namespace}' producing on stream '{streamName}'");
             Console.WriteLine("-------------------------------------------------------------------------------------");

             var owner = CreateApplicationRecord("Dot Net LogEvent Producer");
             var context = new Dictionary<string, object> {{"some key", "some value"}};
             using (var producer = new AxualProducerBuilder<GenericRecord, GenericRecord>(config)
                 .SetKeySerializer(new GenericAvroSerializer<GenericRecord>())
                 .SetValueSerializer(new GenericAvroSerializer<GenericRecord>())
                 .SetLogHandler((_, l) => Console.WriteLine($"> [{l.Level}]: {l.Message}"))
                 .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Reason}"))
                 .Build())
             {
                 // Produce 10 messages
                 for (var i = 0; i < 10; i++)
                 {
                     try
                     {
                         producer.Produce(streamName,
                             CreateMessage(owner, "app_" + i, 2000, "INFO", "Message " + i, context),
                             r => Console.WriteLine(!r.Error.IsError
                                 ? $"> Produced message to stream {r.Topic} partition {r.Partition} offset {r.Offset}"
                                 : $"> Delivery Error: {r.Error.Reason}"));
                     }
                     catch (Exception ex)
                     {
                         Console.WriteLine(ex);
                     }
                 }

                 producer.Flush();
             }
             
             Console.WriteLine("> Done");
         }
         
         private static Message<GenericRecord, GenericRecord> CreateMessage(GenericRecord owner, string applicationName,
             long timestamp, string logLevel, string message, Dictionary<string, object> context)
         {
             var key = CreateApplicationRecord(applicationName, "1.9.9", "none");
             var value = CreateApplicationLogEventRecord(owner, timestamp, logLevel, message, context);

             return new Message<GenericRecord, GenericRecord>
             {
                 Key = key,
                 Value = value
             };
         }

         private static GenericRecord CreateApplicationRecord(string applicationName, string version = null,
             string owner = null)
         {
             var application = new GenericRecord(_applicationSchema);
             application.Add("name", applicationName);
             application.Add("version", version);
             application.Add("owner", owner);
             return application;
         }

         private static GenericRecord CreateApplicationLogEventRecord(GenericRecord application, long timestamp,
             string logLevel, string message, Dictionary<string, object> context)
         {
             var levelSchema = (EnumSchema) _applicationLogEventSchema["level"].Schema;

             var logEvent = new GenericRecord(_applicationLogEventSchema);
             logEvent.Add("timestamp", timestamp);
             logEvent.Add("source", application);
             if (logLevel != null)
             {
                 var enumLogLevel = new GenericEnum(levelSchema, logLevel);
                 logEvent.Add("level", enumLogLevel);
             }

             logEvent.Add("message", message);
             logEvent.Add("context", context);

             return logEvent;
         }
     }
 }