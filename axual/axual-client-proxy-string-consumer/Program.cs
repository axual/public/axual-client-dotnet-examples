﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

 using System;
 using System.IO;
 using System.Threading;
 using Axual.Kafka.Proxy.Proxies.Axual;
 using Confluent.Kafka;
 using Settings;

 namespace axual_client_proxy_string_consumer
{
    public class Program
    {
        public static void Main()
        {
            var serverSetting = new ServerSetting(PlatformType.Standalone);
            
            const string streamName = "string_applicationlog";
            var config = new AxualConsumerConfig
            {
                ApplicationId = "io.axual.example.client.string.consumer",
                EndPoint = serverSetting.EndPoint,
                Environment = serverSetting.Environment,
                Tenant = serverSetting.Tenant,
                
                // SSL Settings:
                SecurityProtocol = SecurityProtocol.Ssl,
                EnableSslCertificateVerification = false,
                
                // Server verifies the identity of the client
                // (i.e. that the public key certificate provided by the client has been signed by a
                // CA trusted by the server).
                // Option 1: As path
                SslCaLocation = serverSetting.SslCaPath,
                // Option 2: As a PEM file format
                //SslCaLocation = File.ReadAllText(serverSetting.SslCaPath),

                // Setting client public and private keys
                // Option A: Using p12 files
                SslKeystorePassword = serverSetting.KeystorePassword,
                SslKeystoreLocation = serverSetting.ConsumerKeystoreResourcePath,
                // Option B: Using PEM files path
                //SslCertificateLocation =  serverSetting.SslConsumerCertificateLocation,
                //SslKeyLocation =  serverSetting.SslConsumerKeyLocation,
                // Option C: Using PEM format string
                //SslCertificatePem =  File.ReadAllText(serverSetting.SslConsumerCertificateLocation),
                //SslKeyPem =  File.ReadAllText(serverSetting.SslConsumerKeyLocation),
                
                //Debug = "all"
            };

            RunConsumer(config, streamName);
        }

        private static void RunConsumer(ConsumerConfig config, string streamName)
        {
            
            var cts = new CancellationTokenSource();
            Console.CancelKeyPress += (_, e) =>
            {
                e.Cancel = true; // prevent the process from terminating.
                cts.Cancel();
            };
            
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine($"  '{typeof(Program).Namespace}' consuming from stream '{streamName}'");
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine("Starting consumer, Ctrl-C to stop consuming");
            
            using (var consumer = new AxualConsumerBuilder<string, string>(config)
                .SetKeyDeserializer(Deserializers.Utf8)
                .SetValueDeserializer(Deserializers.Utf8)
                .SetLogHandler((_, l) => Console.WriteLine($"> [{l.Level}]: {l.Message}"))
                .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Reason}"))
                .Build())
            {
                consumer.Subscribe(streamName);

                try
                {
                    while (!cts.IsCancellationRequested)
                    {
                        try
                        {
                            var consumeResult = consumer.Consume(cts.Token);

                            if (consumeResult.IsPartitionEOF)
                            {
                                Console.WriteLine(
                                    $"> Reached end of stream {consumeResult.Topic}, partition " +
                                    $"{consumeResult.Partition}, offset {consumeResult.Offset}.");

                                continue;
                            }

                            Console.WriteLine(
                                $"> Received message key '{consumeResult.Message.Key}' value '{consumeResult.Message.Value}'" +
                                $"at {consumeResult.TopicPartitionOffset}: ");

                            try
                            {
                                consumer.Commit(consumeResult);
                            }
                            catch (KafkaException e)
                            {
                                Console.WriteLine($"> Commit error: {e.Error.Reason}");
                            }
                        }
                        catch (ConsumeException e)
                        {
                            Console.WriteLine($"> Consume error: {e.Error.Reason}");
                        }
                    }
                }
                finally
                {
                    Console.WriteLine("> Closing consumer");
                    consumer.Close();
                }
                
                Console.WriteLine("> Done");
            }
        }
    }
}