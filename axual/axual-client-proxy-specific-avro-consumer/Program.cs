﻿using System;
using System.IO;
using System.Threading;
using Axual.Kafka.Proxy.Proxies.Axual;
using Axual.SchemaRegistry.Serdes.Avro.Deserializers;
using Confluent.Kafka;
using io.axual.client.example.schema;
using Settings;

namespace axual_client_proxy_specific_avro_consumer
{
    public class Program
    {
        public static void Main()
        {
            var serverSetting = new ServerSetting(PlatformType.Standalone);
            
            const string streamName = "applicationlogevents";
            var config = new AxualConsumerConfig
            {
                ApplicationId = "io.axual.example.client.avro.consumer",
                EndPoint = serverSetting.EndPoint,
                Environment = serverSetting.Environment,
                Tenant = serverSetting.Tenant,

                // SSL Settings
                SecurityProtocol = SecurityProtocol.Ssl, 
                EnableSslCertificateVerification = false,
                 
                // Server verifies the identity of the client
                // (i.e. that the public key certificate provided by the client has been signed by a
                // CA trusted by the server).
                // Option 1: As path
                SslCaLocation = serverSetting.SslCaPath,
                // Option 2: As a PEM file format
                //SslCaLocation = File.ReadAllText(serverSetting.SslCaPath),

                // Setting client public and private keys
                // Option A: Using p12 files
                SslKeystorePassword = serverSetting.KeystorePassword,
                SslKeystoreLocation = serverSetting.ConsumerKeystoreResourcePath,
                // Option B: Using PEM files path
                //SslCertificateLocation =  serverSetting.SslConsumerCertificateLocation,
                //SslKeyLocation =  serverSetting.SslConsumerKeyLocation,
                // Option C: Using PEM format string
                // SslCertificatePem =  File.ReadAllText(serverSetting.SslConsumerCertificateLocation),
                // SslKeyPem =  File.ReadAllText(serverSetting.SslConsumerKeyLocation),
                
                //Debug = "all"
            };
            
            var cts = new CancellationTokenSource();
            Console.CancelKeyPress += (_, e) =>
            {
                e.Cancel = true; // prevent the process from terminating.
                cts.Cancel();
            };

            RunConsumer(config, streamName, cts.Token);
        }

        private static void RunConsumer(ConsumerConfig config, string streamName,
            CancellationToken cancellationToken)
        {
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine($"'{typeof(Program).Namespace}' consuming from stream '{streamName}'");
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine("Started consumer, Ctrl-C to stop consuming");

            using (var consumer = new AxualConsumerBuilder<Application, ApplicationLogEvent>(config)
                .SetKeyDeserializer(new SpecificAvroDeserializer<Application>())
                .SetValueDeserializer(new SpecificAvroDeserializer<ApplicationLogEvent>())
                .SetLogHandler((_, l) => Console.WriteLine($"> [{l.Level}]: {l.Message}"))
                .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Reason}"))
                .Build())
            {
                consumer.Subscribe(streamName);

                try
                {
                    while (true)
                    {
                        try
                        {
                            var consumeResult = consumer.Consume(cancellationToken);

                            if (consumeResult.IsPartitionEOF)
                            {
                                Console.WriteLine(
                                    $"> Reached end of stream {consumeResult.Topic}, partition " +
                                    $"{consumeResult.Partition}, offset {consumeResult.Offset}.");

                                continue;
                            }

                            Console.WriteLine(
                                $"> Received message at {consumeResult.TopicPartitionOffset}: " + Environment.NewLine +
                                $"Key: {consumeResult.Message.Key}" + Environment.NewLine +
                                $"Value: {consumeResult.Message.Value}");

                            try
                            {
                                consumer.Commit(consumeResult);
                            }
                            catch (KafkaException e)
                            {
                                Console.WriteLine($"> Commit error: {e.Error.Reason}");
                            }
                        }
                        catch (ConsumeException e)
                        {
                            Console.WriteLine($"> Consume error: {e.Error.Reason}");
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("> Closing consumer.");
                    consumer.Close();
                }
            }
        }
    }
}