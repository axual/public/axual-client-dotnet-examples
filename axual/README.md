# Axual Client .NET Example Project

This example repository shows a typical use of the different types of clients available in the Axual Platform.
All examples use the same use-case and resources, making it easier to compare them.

## Example Applications

### Goals
For the examples several goals are identified

1. Produce and Consume events using Axual Client Proxy library
2. Start a custom Axual Platform Test standalone, using custom Server and Client Certificates

### Project Structure
The examples in this solution folder user 'Axual.Kafka.Proxy'- a low level client based on Kafka interfaces and programming model.
Below you can find each example use-case and a brief description of each approach.

| Example                                                                                                    |   Client    | Authentication Method | Description                                                                |
|------------------------------------------------------------------------------------------------------------|:-----------:|:---------------------:|----------------------------------------------------------------------------|
| [axual-client-proxy-generic-avro-consumer](axual/axual-client-proxy-generic-avro-consumer)                 |    Axual    |          SSL          | Consumes from stream `applicationlogevents` using generic avro schema      |
| [axual-client-proxy-generic-avro-producer](axual/axual-client-proxy-generic-avro-producer)                 |    Axual    |          SSL          | Produces to stream `applicationlogevents` using generic avro schema        |
| [axual-client-proxy-specific-avro-consumer](axual/axual-client-proxy-specific-avro-consumer)               |    Axual    |          SSL          | Consumes from stream `applicationlogevents` using specific avro schema     |
| [axual-client-proxy-specific-avro-producer](axual/axual-client-proxy-specific-avro-producer)               |    Axual    |          SSL          | Produces to stream `applicationlogevents` using specific avro schema       |
| [axual-client-proxy-string-consumer](axual/axual-client-proxy-string-consumer)                             |    Axual    |          SSL          | Consumes from stream `string_applicationlog`                               |
| [axual-client-proxy-string-producer](axual/axual-client-proxy-string-producer)                             |    Axual    |          SSL          | Produces to stream `string_applicationlog`                                 |
| [axual-client-proxy-string-transactional-producer](axual/axual-client-proxy-string-transactional-producer) |    Axual    |          SSL          | Produces to stream `string_applicationlog` using transactions              |

* [Settings](Settings)\
  Contains settings/configurations shared by example applications and standalone server
  Server and Client SSL Certificates are also stored here

## How to Run
Before running the examples you need to run the standalone container.

Run the following to start standalone prior to running the examples:
```shell
cd Settings/standalone
docker-compose up -d
```

Check the logs with the docker-compose command
Start the consumer application to start consuming messages.
Start the producer application to start producing messages.

## License
Axual Client .NET Example is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
