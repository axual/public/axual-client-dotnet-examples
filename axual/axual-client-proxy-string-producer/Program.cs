﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

 using System;
 using System.IO;
 using System.Numerics;
 using System.Threading;
 using Axual.Kafka.Proxy.Proxies.Axual;
 using Confluent.Kafka;
 using Settings;

namespace axual_client_proxy_string_producer
 {
     public class Program
     {
         public static void Main()
         {
             var serverSetting = new ServerSetting(PlatformType.Standalone);

             const string streamName = "string_applicationlog";
             var config = new AxualProducerConfig
             {
                 ApplicationId = "io.axual.example.client.string.producer",
                 EndPoint = serverSetting.EndPoint,
                 Environment = serverSetting.Environment,
                 Tenant = serverSetting.Tenant,
                 
                 // SSL Settings
                 SecurityProtocol = SecurityProtocol.Ssl,
                 EnableSslCertificateVerification = false,
                 
                 // Server verifies the identity of the client
                 // (i.e. that the public key certificate provided by the client has been signed by a
                 // CA trusted by the server).
                 // Option 1: As path
                 SslCaLocation = serverSetting.SslCaPath,
                 // Option 2: As a PEM file format
                 //SslCaLocation = File.ReadAllText(serverSetting.SslCaPath),
                 
                 
                 // Setting client public and private keys
                 // Option A: Using p12 files
                 SslKeystorePassword = serverSetting.KeystorePassword,
                 SslKeystoreLocation = serverSetting.ProducerKeystoreResourcePath,
                 // Option B: Using PEM files path
                 //SslCertificateLocation =  serverSetting.SslProducerCertificateLocation,
                 //SslKeyLocation =  serverSetting.SslProducerKeyLocation,
                 // Option C: Using PEM format string
                 //SslCertificatePem =  File.ReadAllText(serverSetting.SslProducerCertificateLocation),
                 //SslKeyPem =  File.ReadAllText(serverSetting.SslProducerKeyLocation),
                 
                 //Debug = "all"
             };
             RunProducer(config, streamName);
         }

         private static void RunProducer(ProducerConfig config, string streamName)
         {
             var cts = new CancellationTokenSource();
             Console.CancelKeyPress += (_, e) =>
             {
                 e.Cancel = true; // prevent the process from terminating.
                 cts.Cancel();
             };
             
             Console.WriteLine();
             Console.WriteLine("-------------------------------------------------------------------------------------");
             Console.WriteLine($"  '{typeof(Program).Namespace}' producing to stream '{streamName}'");
             Console.WriteLine("-------------------------------------------------------------------------------------");
             Console.WriteLine("Starting producer, Ctrl-C to stop producing");

             using (var producer = new AxualProducerBuilder<string, string>(config)
                 .SetKeySerializer(Serializers.Utf8)
                 .SetValueSerializer(Serializers.Utf8)
                 .SetLogHandler((_, l) => Console.WriteLine($"> [{l.Level}]: {l.Message}"))
                 .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Reason}"))
                 .Build())
             {
                 var id = new BigInteger(0);

                 while (!cts.IsCancellationRequested)
                 {
                     var key = id;
                     var val = id;
                     id++;

                     try
                     {
                         producer.Produce(
                             streamName, new Message<string, string> {Key = key.ToString(), Value = val.ToString()},
                             r => Console.WriteLine(!r.Error.IsError
                                 ? $"> Produced message to stream {r.Topic} partition {r.Partition} offset {r.Offset}"
                                 : $"> Delivery Error: {r.Error.Reason}"));
                     }
                     catch (Exception ex)
                     {
                         Console.WriteLine(ex);
                     }

                     Thread.Sleep(100);
                 }

                 producer.Flush();
             }
             
             Console.WriteLine("> Done");
         }
     }
 }