﻿using System;
using System.Collections.Generic;
using System.IO;
using Axual.Kafka.Proxy.Proxies.Axual;
using Axual.SchemaRegistry.Serdes.Avro.Serializers;
using Confluent.Kafka;
using Settings;
using io.axual.client.example.schema;


namespace axual_client_proxy_specific_avro_producer
{
    public class Program
    {
        public static void Main()
        {
            var serverSetting = new ServerSetting(PlatformType.Standalone);

            const string streamName = "applicationlogevents";
            var config = new AxualProducerConfig
            {
                ApplicationId = "io.axual.example.client.avro.producer",
                EndPoint = serverSetting.EndPoint,
                Environment = serverSetting.Environment,
                Tenant = serverSetting.Tenant,

                // SSL Settings:
                SecurityProtocol = SecurityProtocol.Ssl,
                EnableSslCertificateVerification = false,
                 
                // Server verifies the identity of the client
                // (i.e. that the public key certificate provided by the client has been signed by a
                // CA trusted by the server).
                // Option 1: As path
                SslCaLocation = serverSetting.SslCaPath,
                // Option 2: As a PEM file format
                //SslCaLocation = File.ReadAllText(serverSetting.SslCaPath),
                 
                // Setting client public and private keys
                // Option A: Using p12 files
                SslKeystorePassword = serverSetting.KeystorePassword,
                SslKeystoreLocation = serverSetting.ProducerKeystoreResourcePath,
                // Option B: Using PEM files path
                //SslCertificateLocation =  serverSetting.SslProducerCertificateLocation,
                //SslKeyLocation =  serverSetting.SslProducerKeyLocation,
                // Option C: Using PEM format string
                //SslCertificatePem =  File.ReadAllText(serverSetting.SslProducerCertificateLocation),
                //SslKeyPem =  File.ReadAllText(serverSetting.SslProducerKeyLocation),
                
                //Debug = "all"
            };

            RunProducer(config, streamName);
        }


        private static void RunProducer(ProducerConfig config, string streamName)
        {
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine($"'{typeof(Program).Namespace}' producing to stream '{streamName}'");
            Console.WriteLine("--------------------------------------------------------------------------------------");

            var application = new Application
            {
                name = "Axual Proxy .NET Specific Avro Producer",
                version = "1.9.9",
                owner = "Team Log"
            };

            using (var producer = new AxualProducerBuilder<Application, ApplicationLogEvent>(config)
                .SetKeySerializer(new SpecificAvroSerializer<Application>())
                .SetValueSerializer(new SpecificAvroSerializer<ApplicationLogEvent>())
                .SetLogHandler((_, l) => Console.WriteLine($"> [{l.Level}]: {l.Message}"))
                .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Reason}"))
                .Build())
            {
                for (var i = 0; i < 10; i++)
                {
                    try
                    {
                        var applicationLogEvent = new ApplicationLogEvent
                        {
                            timestamp = 1000 + i,
                            source = application,
                            message = "Message " + i,
                            context = new Dictionary<string, string> {{$"some key {i}", $"some value {i}"}},
                            level = ApplicationLogLevel.INFO
                        };

                        var message = new Message<Application, ApplicationLogEvent>
                        {
                            Key = application,
                            Value = applicationLogEvent
                        };

                        producer.Produce(streamName, message,
                            r => Console.WriteLine(!r.Error.IsError
                                ? $"> Produced message to stream {r.Topic} partition {r.Partition} offset {r.Offset}"
                                : $"> Delivery Error: {r.Error.Reason}"));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }

                producer.Flush();
            }

            Console.WriteLine("> Done");
        }
    }
}