# Client .NET Examples for Axual Platform

This example repository shows a typical use of the different types of clients available in the Axual Platform.
All examples use the same use-case and resources, making it easier to compare them.

## Goals
For the examples several goals are identified

1. Produce and Consume events using Axual Client Proxy library
2. Start a custom Axual Platform Test standalone, using custom Server and Client Certificates
3. Produce and Consuåme events from Axual Platform using the Confluent Kafka and Schema Registry libraries

## Project Structure
The examples are split into different solution directories.
Check the ReadMe files in each for more information on how to run the projects.

[Kafka](kafka/README.md) For examples using Confluent Kafka Client libraries.

Note: At this time, SASL authentication is only supported using Confluent Kafka Client

## EOL notice
Please notice that the Axual Client(s) will reach EOL by the end of 2024. Read more about this in the following blog post: https://axual.com/update-on-support-of-axual-client-libraries

## License
Client .NET Examples for Axual Platform is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
