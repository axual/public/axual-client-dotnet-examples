Apache Axual .NET Client Example
Copyright Axual B.V. The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

This product bundles EndianBitConverter 1.1.0, which is available under a "MIT" license. For details, see https://github.com/davidrea-MS/BitConverter/blob/master/LICENSE.txt.

This product bundles Confluent.Kafka 1.2.0, which is available under a "Apache License 2.0" license. For details, see https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/LICENSE.

This product bundles Confluent.SchemaRegistry 1.2.0, which is available under a "Apache License 2.0" license. For details, see https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/LICENSE.

This product bundles Confluent.SchemaRegistry.Serdes 1.2.0, which is available under a "Apache License 2.0" license. For details, see https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/LICENSE.


This product bundles RichardSzalay.MockHttp 5.0.0, which is available under a "MIT" license. For details, see https://github.com/richardszalay/mockhttp/blob/master/LICENSE.
