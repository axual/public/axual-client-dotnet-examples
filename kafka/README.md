# Confluent Client .NET Example on Axual Platform

The examples in this solution folder contain examples on how to connect to Axual Platform using the Confluent Kafka client libraries.
All examples use the same use-case and resources, making it easier to compare them.

## Example Applications

### Goals
For the examples several goals are identified

1. Produce and Consume events from Axual Platform using the Confluent Kafka and Schema Registry libraries
2. Demonstrate how to configure the clients to authenticate against the Axual Platform components using SASL and TLS with client certificates

### Project Structure
Below you can find each example use-case and a brief description of each approach.

| Example                                                                                                    |   Client    | Authentication Method | Description                                                                |
|------------------------------------------------------------------------------------------------------------|:-----------:|:---------------------:|----------------------------------------------------------------------------|
| [kafka-client-sasl-avro-consumer](kafka/kafka-client-sasl-avro-consumer)                                   | Plain Kafka |         SASL          | Consumes from the `applicationlogevents` stream, using generic avro schema |
| [kafka-client-sasl-avro-producer](kafka/kafka-client-sasl-avro-producer)                                   | Plain Kafka |         SASL          | Produces to the `applicationlogevents` stream, using generic avro schema   |
| [kafka-client-sasl-string-consumer](kafka/kafka-client-sasl-string-consumer)                               | Plain Kafka |         SASL          | Consumes from stream `string_applicationlog`                               |
| [kafka-client-sasl-string-producer](kafka/kafka-client-sasl-string-producer)                               | Plain Kafka |         SASL          | Produces to stream `string_applicationlog`                                 |
| [kafka-client-ssl-avro-consumer](kafka/kafka-client-ssl-avro-consumer)                                     | Plain Kafka |          SSL          | Consumes from stream `applicationlogevents`, using generic avro schema     |
| [kafka-client-ssl-avro-producer](kafka/kafka-client-ssl-avro-producer)                                     | Plain Kafka |          SSL          | Produces to stream `applicationlogevents`, using generic avro schema       |
| [kafka-client-ssl-string-consumer](kafka/kafka-client-ssl-string-consumer)                                 | Plain Kafka |          SSL          | Produces to stream `string_applicationlog`                                 |
| [kafka-client-ssl-string-producer](kafka/kafka-client-ssl-string-producer)                                 | Plain Kafka |          SSL          | Consumes from stream `string_applicationlog`                               |

## How to Run
Before running the examples you need to update the examples to match your stream and application registration with Axual Self Service.

1. If you don't have an installation of the platform available, register for a SaaS Trial at [Axual](https://axual.com/trial/https://axual.com/trial/)
2. Follow the Getting Started guide in the [Axual Doccumentation](https://docs.axual.io/)
3. Update in your example the following variables, if defined:
   1. tenant
   2. instance
   3. environment
   4. streamName (if you diverged from the Getting Started names)
   5. applicationId (if you diverged from the Getting Started names)
   6. Update credentials to match the ones defined in Self Service
      1. SSL
         1. Path to file with trusted certificate authorities 
         2. Path to Certificate file in the Producer/Consumer configs
         3. Path to Key filein the Producer/Consumer configs
         4. (Avro examples only) Path to PKCS #12 keystore file (extension _*.p12*_ ) 
         5. (Avro examples only) Schema Registry URL
      2. SASL 
         1. username
         2. password
         3. Path to file with trusted certificate authorities
         4. (Avro examples only) Schema Registry URL


## License
Axual Client .NET Example is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
