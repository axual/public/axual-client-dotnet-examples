﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//

using System;
using System.IO;
using System.Numerics;
using System.Threading;
using Confluent.Kafka;

namespace kafka_client_ssl_string_producer
{
    class Program
    {
        private static readonly string RootFolder =
            Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName;

        static void Main(string[] args)
        {
            // The stream name defined in Axual Self Service where we want to produce
            const string streamName = "string_applicationlog";
            
            // Axual uses namespacing to enable tenants, instance and environments to share the same cluster
            // Determine the axual specific settings, to be translated to Kafka resource names
            const string tenant = "demo";
            const string instance = "local";
            const string environment = "example";

            // Determine the full topic name based on the topic pattern
            // The default pattern is "{tenant}-{instance}-{environment}-{streamName}"
            // The naming pattern can be retrieved from the Discovery API or from the support team for your deployment
            var topic = $"{tenant}-{instance}-{environment}-{streamName}";

            const string applicationId = "io.axual.example.client.avro.producer";
            // Determine the client id based on the group pattern
            // The default pattern is "{tenant}-{instance}-{environment}-{applicationId}"
            var clientId = $"{tenant}-{instance}-{environment}-{applicationId}";

            var config = new ProducerConfig()
            {
                // Connect to the SSL listener of the local platform
                BootstrapServers = "platform.local:9096",

                // Set the security protocol to use SSL and certificate based authentication
                SecurityProtocol = SecurityProtocol.Ssl,
                
                // Ssl settings
                SslCertificateLocation =  $"{RootFolder}/certificates/example_producer.cer",
                SslKeyLocation =  $"{RootFolder}/certificates/example_producer.key",
                SslCaLocation = $"{RootFolder}/certificates/trustedCa.pem",
                EnableSslCertificateVerification = false,
                
                // Specify the Kafka delivery settings 
                Acks = Acks.All,
                LingerMs = 10,

                //Debug = "all"

                // Specify the Kafka ClientID settings
                ClientId = clientId
            };
            
            RunProducer(config, topic);
        }

         private static void RunProducer(ProducerConfig config, string topic)
         {
             var cts = new CancellationTokenSource(TimeSpan.FromSeconds(15));
             Console.CancelKeyPress += (_, e) =>
             {
                 e.Cancel = true; // prevent the process from terminating.
                 cts.Cancel();
             };
             
             Console.WriteLine();
             Console.WriteLine("-------------------------------------------------------------------------------------");
             Console.WriteLine($"  '{typeof(Program).Namespace}' producing to stream '{topic}'");
             Console.WriteLine("-------------------------------------------------------------------------------------");
             Console.WriteLine("Starting producer, Ctrl-C to stop producing");

             using (var producer = new ProducerBuilder<string, string>(config)
                 .SetKeySerializer(Serializers.Utf8)
                 .SetValueSerializer(Serializers.Utf8)
                 .SetLogHandler((_, l) => Console.WriteLine($"> [{l.Level}]: {l.Message}"))
                 .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Reason}"))
                 .Build())
             {
                 var id = new BigInteger(0);

                 while (!cts.IsCancellationRequested)
                 {
                     var key = id.ToString();
                     var val = $"{typeof(Program).Namespace} produced {id}";
                     id++;

                     try
                     {
                         producer.Produce(
                             topic, new Message<string, string> {Key = key, Value = val},
                             r => Console.WriteLine(!r.Error.IsError
                                 ? $"> Produced message: key '{key}' value '{val}' to stream {r.Topic} partition {r.Partition} offset {r.Offset}"
                                 : $"> Delivery Error: {r.Error.Reason}"));
                     }
                     catch (Exception ex)
                     {
                         Console.WriteLine(ex);
                     }

                     Thread.Sleep(100);
                 }

                 producer.Flush();
             }
             
             Console.WriteLine("> Done");
         }
     }    
}
