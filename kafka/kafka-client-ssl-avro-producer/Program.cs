﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Threading;
using Avro;
using Avro.Generic;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;
using Schema = Avro.Schema;

namespace kafka_client_ssl_avro_producer
{
    class Program
    {
        private static readonly string RootFolder =
            Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName;

        private static readonly RecordSchema ApplicationSchema = 
            (RecordSchema)Schema.Parse(File.ReadAllText(RootFolder + "/schemas/Application.avsc"));
        private static readonly  RecordSchema ApplicationLogEventSchema = 
            (RecordSchema)Schema.Parse(File.ReadAllText(RootFolder + "/schemas/ApplicationLogEvent.avsc"));

        static void Main(string[] args)
        {
            // The stream name defined in Axual Self Service where we want to produce
            const string streamName = "applicationlogevents";
            
            // Axual uses namespacing to enable tenants, instance and environments to share the same cluster
            // Determine the axual specific settings, to be translated to Kafka resource names
            const string tenant = "demo";
            const string instance = "local";
            const string environment = "example";
            
            // Determine the full topic name based on the topic pattern
            // The default pattern is "{tenant}-{instance}-{environment}-{streamName}"
            // The naming pattern can be retrieved from the Discovery API or from the support team for your deployment
            var topic = $"{tenant}-{instance}-{environment}-{streamName}";

            const string applicationId = "io.axual.example.client.avro.producer";
            // Determine the client id based on the group pattern
            // The default pattern is "{tenant}-{instance}-{environment}-{applicationId}"
            var clientId = $"{tenant}-{instance}-{environment}-{applicationId}";

            var producerConfig = new ProducerConfig()
            {
                // Connect to the SSL listener of the local platform
                BootstrapServers = "platform.local:9096",

                // Set the security protocol to use SSL and certificate based authentication
                SecurityProtocol = SecurityProtocol.Ssl,
                
                // Ssl settings
                // Keystore files are used because Schema Registry client does not support PEM files
                SslKeystoreLocation = $"{RootFolder}/certificates/example-producer.p12", 
                SslKeystorePassword = "notsecret",
                SslCaLocation = $"{RootFolder}/certificates/trustedCa.pem",
                EnableSslCertificateVerification = false,

                // Specify the Kafka delivery settings 
                Acks = Acks.All,
                LingerMs = 10,

                //Debug = "all"

                // Specify the Kafka ClientID settings
                ClientId = clientId
            };

            var schemaRegistryConfig = new SchemaRegistryConfig()
            {
                Url = "https://platform.local:24000/",
                
                // Ssl settings for trusting the Schema Registry, and identifying with certificates
                // Keystore files are used because Schema Registry client does not support PEM files
                SslKeystoreLocation = $"{RootFolder}/certificates/example-producer.p12", 
                SslKeystorePassword = "notsecret",
                SslCaLocation = $"{RootFolder}/certificates/trustedCa.pem",
                EnableSslCertificateVerification = false,
            };
            
            RunProducer(producerConfig,schemaRegistryConfig, topic);
        }

        private static void RunProducer(ProducerConfig producerConfig, SchemaRegistryConfig schemaRegistryConfig, string topic)
         {
             // Construct the Schema Registry Client and serializer
             var srClient = new CachedSchemaRegistryClient(schemaRegistryConfig);
             var avroSerializer = new AvroSerializer<GenericRecord>(srClient).AsSyncOverAsync();
             
             var cts = new CancellationTokenSource(TimeSpan.FromSeconds(15));
             Console.CancelKeyPress += (_, e) =>
             {
                 e.Cancel = true; // prevent the process from terminating.
                 cts.Cancel();
             };
             
             Console.WriteLine();
             Console.WriteLine("-------------------------------------------------------------------------------------");
             Console.WriteLine($"  '{typeof(Program).Namespace}' producing to stream '{topic}'");
             Console.WriteLine("-------------------------------------------------------------------------------------");
             Console.WriteLine("Starting producer, Ctrl-C to stop producing");

             using (var producer = new ProducerBuilder<GenericRecord, GenericRecord>(producerConfig)
                 .SetKeySerializer(avroSerializer)
                 .SetValueSerializer(avroSerializer)
                 .SetLogHandler((_, l) => Console.WriteLine($"> [{l.Level}]: {l.Message}"))
                 .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Reason}"))
                 .Build())
             {
                 var id = new BigInteger(0);
                 var appName = "Example Avro SSL Producer";
                 var owner = "Axual Demo";
                 var logContext = new Dictionary<string, object> {{"some key", "some value"}};
                 var logLevel = "INFO";

                 while (!cts.IsCancellationRequested)
                 {
                     var logMessage = $"Logging for producer, iteration {id}";
                     // By changing the version field the target partition will change on the topic
                     var key = CreateApplicationRecord(appName, id.ToString(), owner);
                     var val = CreateApplicationLogEventRecord(key, DateTime.Now.ToFileTimeUtc(), logLevel,logMessage,logContext);
                     id++;

                     try
                     {
                         producer.Produce(
                             topic, new Message<GenericRecord, GenericRecord> {Key = key, Value = val},
                             r => Console.WriteLine(!r.Error.IsError
                                 ? $"> Produced message: log message '{logMessage}' to stream {r.Topic} partition {r.Partition} offset {r.Offset}"
                                 : $"> Delivery Error: {r.Error.Reason}"));
                     }
                     catch (Exception ex)
                     {
                         Console.WriteLine(ex);
                     }

                     Thread.Sleep(100);
                 }

                 producer.Flush();
             }
             
             Console.WriteLine("> Done");
         }
        
        // Construct an Avro Generic Record based on the Application schema definition
        private static GenericRecord CreateApplicationRecord(string applicationName, string version = null,
            string owner = null)
        {
            var application = new GenericRecord(ApplicationSchema);
            application.Add("name", applicationName);
            application.Add("version", version);
            application.Add("owner", owner);
            return application;
        }
        
        // Construct an Avro Generic Record based on the ApplicationLogEvent schema definition
        private static GenericRecord CreateApplicationLogEventRecord(GenericRecord application, long timestamp,
            string logLevel, string message, Dictionary<string, object> context)
        {
            var levelSchema = (EnumSchema) ApplicationLogEventSchema["level"].Schema;

            var logEvent = new GenericRecord(ApplicationLogEventSchema);
            logEvent.Add("timestamp", timestamp);
            logEvent.Add("source", application);
            if (logLevel != null)
            {
                var enumLogLevel = new GenericEnum(levelSchema, logLevel);
                logEvent.Add("level", enumLogLevel);
            }

            logEvent.Add("message", message);
            logEvent.Add("context", context);

            return logEvent;
        }
    }    
}
