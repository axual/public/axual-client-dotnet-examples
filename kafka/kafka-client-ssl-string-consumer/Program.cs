﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.IO;
using System.Threading;
using Confluent.Kafka;

namespace kafka_client_ssl_string_consumer
{
    public class Program
    {
        private static readonly string RootFolder =
            Directory.GetParent(Environment.CurrentDirectory)
                .Parent.Parent.FullName;
        
        public static void Main()
        {
            // The stream name defined in Axual Self Service where we want to produce
            const string streamName = "string_applicationlog";
            const string applicationId = "io.axual.example.client.string.consumer";
            
            // Axual uses namespacing to enable tenants, instance and environments to share the same cluster
            // Determine the axual specific settings, to be translated to Kafka resource names
            const string tenant = "demo";
            const string instance = "local";
            const string environment = "example";
            
            // Determine the full topic name based on the topic pattern
            // The default pattern is "{tenant}-{instance}-{environment}-{streamName}"
            // The naming pattern can be retrieved from the Discovery API or from the support team for your deployment
            var topic = $"{tenant}-{instance}-{environment}-{streamName}";

            // Determine the full group id based on the group pattern
            // The default pattern is "{tenant}-{instance}-{environment}-{applicationId}"
            // The naming pattern can be retrieved from the Discovery API or from the support team for your deployment
            var groupId = $"{tenant}-{instance}-{environment}-{applicationId}";

            // Determine the client id based on the group pattern
            // The default pattern is "{tenant}-{instance}-{environment}-{applicationId}"
            var clientId = $"{tenant}-{instance}-{environment}-{applicationId}";
            
            var config = new ConsumerConfig
            {
                // Connect to the SSL listener of the local platform
                BootstrapServers = "platform.local:9096",

                // Set the security protocol to use SSL and certificate based authentication
                SecurityProtocol = SecurityProtocol.Ssl,
                
                // Ssl settings
                SslCertificateLocation =  $"{RootFolder}/certificates/example_consumer.cer",
                SslKeyLocation =  $"{RootFolder}/certificates/example_consumer.key",
                SslCaLocation = $"{RootFolder}/certificates/trustedCa.pem",
                EnableSslCertificateVerification = false,
                
                // Specify the Kafka delivery settings
                GroupId = groupId,
                EnableAutoCommit = true,
                AutoCommitIntervalMs = 200,
                AutoOffsetReset = AutoOffsetReset.Earliest,

                // Specify the Kafka ClientID settings
                ClientId = clientId
            };

            RunConsumer(config, topic);
        }

        private static void RunConsumer(ConsumerConfig config, string topic)
        {
            
            var cts = new CancellationTokenSource();
            Console.CancelKeyPress += (_, e) =>
            {
                e.Cancel = true; // prevent the process from terminating.
                cts.Cancel();
            };
            
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine($"  '{typeof(Program).Namespace}' consuming from stream '{topic}'");
            Console.WriteLine("--------------------------------------------------------------------------------------");
            Console.WriteLine("Starting consumer, Ctrl-C to stop consuming");
            
            using (var consumer = new ConsumerBuilder<string, string>(config)
                .SetKeyDeserializer(Deserializers.Utf8)
                .SetValueDeserializer(Deserializers.Utf8)
                .SetLogHandler((_, l) => Console.WriteLine($"> [{l.Level}]: {l.Message}"))
                .SetErrorHandler((_, e) => Console.WriteLine($"> [Error]: {e.Code} -{e.Reason} "))
                .Build())
            {
                consumer.Subscribe(topic);

                try
                {
                    while (!cts.IsCancellationRequested)
                    {
                        try
                        {
                            var consumeResult = consumer.Consume(cts.Token);

                            if (consumeResult.IsPartitionEOF)
                            {
                                Console.WriteLine(
                                    $"> Reached end of stream {consumeResult.Topic}, partition " +
                                    $"{consumeResult.Partition}, offset {consumeResult.Offset}.");

                                continue;
                            }

                            Console.WriteLine(
                                $"> Received message key '{consumeResult.Message.Key}' value '{consumeResult.Message.Value}'" +
                                $"at {consumeResult.TopicPartitionOffset}: ");

                            try
                            {
                                consumer.Commit(consumeResult);
                            }
                            catch (KafkaException e)
                            {
                                Console.WriteLine($"> Commit error: {e.Error.Reason}");
                            }
                        }
                        catch (ConsumeException e)
                        {
                            Console.WriteLine($"> Consume error: {e.Error.Reason}");
                        }
                    }
                }
                finally
                {
                    Console.WriteLine("> Closing consumer");
                    consumer.Close();
                }
                
                Console.WriteLine("> Done");
            }
        }
    }
}