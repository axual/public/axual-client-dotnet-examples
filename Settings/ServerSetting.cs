﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.


using System;
using System.IO;

namespace Settings
{
    public class ServerSetting
    {
        private static readonly string RootFolder =
            Directory.GetParent(System.Environment.CurrentDirectory)
                .Parent.Parent.Parent.Parent.FullName + "/Settings";

        public string Tenant { get; }
        public string Instance { get; }
        public string Environment { get; }
        public Uri EndPoint { get; }
        public string ProducerKeystoreResourcePath { get; }
        public string ConsumerKeystoreResourcePath { get; }
        public string KeystorePassword { get; }
        public string SslKeyPassword { get; }
        public string SslCaPath { get; }
        public string SslProducerCertificateLocation { get; }
        public string SslProducerKeyLocation { get; }
        public string SslConsumerCertificateLocation { get; }
        public string SslConsumerKeyLocation { get; }
        public string ApplicationSchemaPath { get; }
        public string ApplicationLogEventSchemaPath { get; }

        public ServerSetting(PlatformType platformType)
        {
            Tenant = "axual";
            Instance = "local";
            Environment = "local";
            ApplicationSchemaPath =
                RootFolder +
                "/avro/io.axual.client.example.schema.Application.dotnet.avsc";
            ApplicationLogEventSchemaPath =
                RootFolder +
                "/avro/io.axual.client.example.schema.ApplicationLogEvent.dotnet.avsc";

            switch (platformType)
            {
                case PlatformType.LocalPlatform:
                {
                    EndPoint = new UriBuilder("https", "platform.local", 443).Uri;
                    ProducerKeystoreResourcePath =
                        RootFolder + "/local-platform/example-producer/p12/example-producer.client.p12";
                    ConsumerKeystoreResourcePath =
                        RootFolder + "/local-platform/example-consumer/p12/example-consumer.client.p12";
                    KeystorePassword = "notsecret";
                    SslKeyPassword = "";
                    SslCaPath = RootFolder + "/local-platform/common-truststore/cachain/common-truststore.pem";
                }
                    break;
                case PlatformType.KubernetesLocalPlatform:
                {
                    Environment = "example";
                    EndPoint = new UriBuilder("https", "platform.local", 29000).Uri;
                    ProducerKeystoreResourcePath =
                        RootFolder + "/local-platform/example-producer/p12/example-producer.client.p12";
                    ConsumerKeystoreResourcePath =
                        RootFolder + "/local-platform/example-consumer/p12/example-consumer.client.p12";
                    KeystorePassword = "notsecret";
                    SslKeyPassword = "";
                    SslCaPath = RootFolder + "/local-platform/common-truststore/cachain/common-truststore.pem";
                    SslProducerCertificateLocation = RootFolder + "/local-platform/example-producer/cer/example_producer.cer";
                    SslProducerKeyLocation =  RootFolder + "/local-platform/example-producer/keys/example_producer.key";
                    SslConsumerCertificateLocation = RootFolder + "/local-platform/example-consumer/cer/example_consumer.cer";
                    SslConsumerKeyLocation =  RootFolder + "/local-platform/example-consumer/keys/example_consumer.key";
                }
                    break;
                case PlatformType.Standalone:
                {
                    EndPoint = new UriBuilder("http", "127.0.0.1", 8081).Uri;
                    ProducerKeystoreResourcePath =
                        RootFolder + "/standalone/configs/axual.client.keystore.p12";
                    ConsumerKeystoreResourcePath =
                        RootFolder + "/standalone/configs/axual.client.keystore.p12";
                    KeystorePassword = "notsecret";
                    SslCaPath = RootFolder + "/standalone/configs/ca-root.cer";
                }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(platformType), platformType, null);
            }
        }
    }

    public enum PlatformType
    {
        LocalPlatform,
        KubernetesLocalPlatform,
        Standalone
    }
}